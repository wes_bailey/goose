package main

import (
	"../../lib/goose"
	"fmt"
	"log"
	"os"
)

var createSchemaCmd = &Command{
	Name:    "create:schema",
	Usage:   "",
	Summary: "Create a migration based off a SQL export of the database schema",
	Help:    "",
	Run:     createSchemaRun,
}

func createSchemaRun(cmd *Command, args ...string) {
	if len(args) < 1 {
		log.Fatal("goose create:schema schemafile.sql")
	}

	conf, err := dbConfFromFlags()
	if err != nil {
		log.Fatal(err)
	}

	err = os.MkdirAll(conf.MigrationsDir, 0777)
	if err != nil {
		log.Fatal(err)
	}

	fn, err := goose.CreateSchemaMigration(args[0], conf.MigrationsDir)

	fmt.Println("goose: schema initialization migration created: ", fn)
}
