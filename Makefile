# Colors
BOLD:=$(shell tput bold)
GREEN:=$(shell tput setaf 2)
BLUE:=$(shell tput setaf 4)

# common functions for print to stdout
complete=@echo ${BOLD}${BLUE}complete
title=@echo ${BOLD}${GREEN}== $(1) ==

GOROOT := $(shell go env GOROOT)
GOPATH := $(shell go env GOPATH)
GOBIN := $(shell go env GOBIN)
CLEAN_DIR := "pkg/$(shell go env GOOS)_$(shell go env GOARCH)"
GOBUILD=go install
DEBUG=-a -v -work -x

.SILENT:

export

all: build

debug: GOBUILD += $(DEBUG)
debug: all

clean:
	- rm $(CLEAN_DIR)/*.a

build:
	$(call title, "Building Goose")
	$(MAKE) build -C cmd/goose
	$(call complete)

list:
	grep '^[a-z]*:' Makefile | grep -v '^list:'
